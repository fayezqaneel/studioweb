function getArticles() {
    return fetch("http://studioweb-app1.getsandbox.com/news", {
        method: 'GET',
        headers: {},
        body: null
    }).then(function(response) {
        if (response.ok) {
            return response.json();
        } else {
            return [];
        }
    }).then(function(responseObject) {
        return responseObject;
    });
}

module.exports = {
    getArticles
};
