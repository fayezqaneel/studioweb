var Observable = require("FuseJS/Observable");
var Backend = require("./Backend");
var DateFormat = require("Modules/DateFormat");

var articles = Observable();

Backend.getArticles()
    .then(function(newArticles) {
        for (var i in newArticles) {
            if (newArticles[i].images.length > 0) {
                newArticles[i].feature_image = newArticles[i].images[0];
            } else {
                newArticles[i].feature_image = "default-image";
            }
            newArticles[i].publishedAt = DateFormat.format(newArticles[i].publishedAt, "mediumDate")
        }
        articles.replaceAll(newArticles);
    })
    .catch(function(error) {
        console.log("Couldn't get articles: " + error);
    });


module.exports = {
    articles
};
