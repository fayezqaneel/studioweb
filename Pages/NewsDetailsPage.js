var article = this.Parameter;
var Observable = require("FuseJS/Observable");
var Share = require("FuseJS/Share")
var Storage = require("FuseJS/Storage");

var favorites_array = [];

var in_favorites = Observable(false);
var id = 0;

var images = article.map(function(x) {
    images_list = Observable();
    for (var i in x.images) {
        images_list.add(x.images[i]);
    }
    return images_list;
});

var short_description = article.map(function(x) { return x.short_description; });
var publishedAt = article.map(function(x) { return x.publishedAt; });
var author = article.map(function(x) { return x.author; });
var description = article.map(function(x) { return x.description; });
var video = article.map(function(x) { return x.video; });
var title = article.map(function(x) { return x.title; });
var category = article.map(function(x) { return x.category; });
var url = article.map(function(x) { return x.url; });

var feature_image = article.map(function(x) { return x.feature_image; });

function goBack() {
    router.goBack();
}

function shareArticle() {
    Share.shareText(url.value, title.value);
}

function addToFavorites() {
    if (favorites_array.indexOf(id) == -1) {
        favorites_array.push(id);
        in_favorites.value = true;
    } else {
        favorites_array.splice(favorites_array.indexOf(id), 1);
        in_favorites.value = false;
    }
    Storage.write("favorites.json", JSON.stringify(favorites_array))
        .then(function(succeeded) {
            if (succeeded) {
                console.log("Successfully wrote to file");
            } else {
                console.log("Couldn't write to file.");
            }
        });
}


function initFavorites() {
    id = router.selected_article_id;
    in_favorites.value = false;
    console.log(JSON.stringify(favorites_array));
    console.log(id);
    if (favorites_array.length == 0) {
        Storage.read("favorites.json")
            .then(function(contents) {
                favorites_array = JSON.parse(contents);
                if (favorites_array.indexOf(id) != -1) {
                    in_favorites.value = true;
                }
            }, function(error) {
                console.log(error);
            });
    } else {

        if (favorites_array.indexOf(id) != -1) {
            in_favorites.value = true;
        }
    }
}


module.exports = {
    images,
    short_description,
    publishedAt,
    author,
    description,
    video,
    title,
    category,
    url,
    feature_image,
    goBack,
    shareArticle,
    in_favorites,
    addToFavorites,
    initFavorites
};
