var Controllers = require("Modules/Controllers");

function goToArticle(arg) {
	var article = arg.data;
	router.selected_article_id = arg.data.id;
	router.push("NewsDetails", article);
}

module.exports = {
	articles: Controllers.articles,
	featured:Controllers.articles.slice(0, 1),
	goToArticle
};